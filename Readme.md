# Aurora Penguinis GRUB2 & Plymouth Theme

Plymouth theme for every linux desktop, based on the work of OobuntusGooftus and
[Brahimsalem](http://gnome-look.org/usermanager/search.php?username=Brahimsalem),
Nigel Swales's [Penguin Photo](https://commons.wikimedia.org/wiki/Spheniscidae#/media/),
[Vinceluice's Grub Themes](https://github.com/vinceliuice/grub2-themes),
and [Virpara](http://virpara.blogspot.com]).

Please refer to the respective Readme's:

[Aurora Penguinis GRUB2 Theme](./Aurora-Penguinis-GRUB2/README.md)

[Aurora Penguinis Plymouth Theme](./Aurora-Penguinis-Plymouth/Readme.md)

[Aurora Penguinis Plymouth 2 Theme](./Aurora-Penguinis-Plymouth-2/Readme.md)

[Starry Night Plymouth Theme](./Starry-Night-Plymouth/Readme.md)


## Screenshots
![alt text](./Aurora-Penguinis-GRUB2/preview.png "GRUB2 Theme")

![alt text](./Aurora-Penguinis-Plymouth/screenshot2.png "Plymouth Theme")

![alt text](./Aurora-Penguinis-Plymouth-2/preview.png "Plymouth Theme 2")

![alt text](./Starry-Night-Plymouth/preview.png "Plymouth Theme 3")
