# Aurora Penguinis Plymouth Theme 2

Aurora Penguinis is a modern, colorful plymouth theme for the bootup of
linux PCs. It consists of an elegant small penguin and a spinner showing
work in progress.


## How to install

**WARNING:** Do not install unless you know what you do and how to repair a non-booting system.

* Download and unpack [Aurora-Penguinis-Plymouth-2.tar.xz](https://bitbucket.org/gemlion/aurora-penguinis/raw/master/Aurora-Penguinis-Plymouth-2.tar.xz "Install package").

* Copy the whole folder **Aurora-Penguinis-Plymouth-2** with root privileges to **/usr/share/plymouth/themes/**

```bash
$ sudo cp -R Aurora-Penguinis-Plymouth-2 /usr/share/plymouth/themes/
```

* Then run the following commands in a terminal:

```bash
$ sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/Aurora-Penguinis-Plymouth-2/aurora-penguinis-2.plymouth 100

$ sudo update-alternatives --config default.plymouth

$ sudo update-initramfs -u
```


## Sources

Aurora Penguinis is based on the Xubuntu Logo Plymouth Theme by the
Xubuntu Developer Team

Background Picture: [Official KDE5.3 Wallpaper](https://projects.kde.org/projects/kde/workspace/breeze/repository/revisions/master/changes/wallpapers/Next/contents/images/1600x1200.png)

