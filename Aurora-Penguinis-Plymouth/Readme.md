# Aurora Penguinis Plymouth Theme

Aurora Penguinis is a modern, colorful plymouth theme for the bootup of
linux PCs. It consists of an elegant small linux text, fading in and out
acompanied by a swimming penguin. The background stays blank cycling
through several colors.


## How to install

**WARNING:** Do not install unless you know what you do and how to repair a non-booting system.

* Download and unpack [Aurora-Penguinis-Plymouth.tar.xz](https://bitbucket.org/gemlion/aurora-penguinis/raw/master/Aurora-Penguinis-Plymouth.tar.xz "Install package").

* Copy the whole folder **Aurora-Penguinis-Plymouth** with root privileges to **/usr/share/plymouth/themes/**

```bash
$ sudo cp -R Aurora-Penguinis-Plymouth /usr/share/plymouth/themes/
```

* Then run the following commands in a terminal:

```bash
$ sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/Aurora-Penguinis-Plymouth/aurora-penguinis.plymouth 100

$ sudo update-alternatives --config default.plymouth

$ sudo update-initramfs -u
```


## Sources

Aurora Penguinis is based on the work of the following artists:

[OobuntusGooftus](http://gnome-look.org/usermanager/search.php?username=OobuntusGooftus) and [Brahimsalem](http://gnome-look.org/usermanager/search.php?username=Brahimsalem)

[Nigel Swales' Penguin Photo](https://commons.wikimedia.org/wiki/Spheniscidae#/media/)

[Virpara](http://virpara.blogspot.com)

